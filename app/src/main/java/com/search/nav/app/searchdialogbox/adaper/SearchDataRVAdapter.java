package com.search.nav.app.searchdialogbox.adaper;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.search.nav.app.searchdialogbox.R;
import com.search.nav.app.searchdialogbox.model.UserDataModel;

import java.util.List;

/**
 * Created by i5tagbin2 on 8/9/17.
 */

public class SearchDataRVAdapter extends RecyclerView.Adapter<SearchDataRVAdapter.MyViewHolder> {
    Context context;
    List<String> getCityList;
    public SearchDataRVAdapter(Context context, List<String> cityModels){
        this.context=context;
        this.getCityList=cityModels;

    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.item_search, parent, false);
        MyViewHolder mvh=new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.cityTv.setText(getCityList.get(position));
    }

    @Override
    public int getItemCount() {
        return getCityList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView cityTv;
        public MyViewHolder(View itemView) {
            super(itemView);
            cityTv= (TextView) itemView.findViewById(R.id.tvAutocomName);
        }
    }
}
