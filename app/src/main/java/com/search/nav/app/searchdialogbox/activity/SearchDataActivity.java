package com.search.nav.app.searchdialogbox.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.search.nav.app.searchdialogbox.R;
import com.search.nav.app.searchdialogbox.adaper.SearchDataRVAdapter;
import com.search.nav.app.searchdialogbox.database.DatabaseBackend;
import com.search.nav.app.searchdialogbox.model.UserDataModel;

import java.util.ArrayList;
import java.util.List;

public class SearchDataActivity extends AppCompatActivity {
    AutoCompleteTextView searchUserTv;
    RecyclerView selectedDataRv;
    SearchDataRVAdapter searchDataAdapter;
    List<String> listItems=new ArrayList<String>();
    List<String> selectedItemList=new ArrayList<>();
    DatabaseBackend databseBackend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_data);
        searchUserTv= (AutoCompleteTextView) findViewById(R.id.actUserAssignSearch);
        selectedDataRv= (RecyclerView) findViewById(R.id.data_rv);
        databseBackend=DatabaseBackend.getInstance(this);
        refreshList("");
    }

    private void refreshList(String s) {
        listItems.clear();
        List<UserDataModel> mContactList= databseBackend.getUserData("");
        for(int i=0;i<mContactList.size();i++)
        {
            listItems.add(mContactList.get(i).getName());
        }
        final ArrayAdapter<String> adapter =new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, listItems);
        searchUserTv.setAdapter(adapter);
        searchUserTv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedItemList.add(adapter.getItem(i));
                searchUserTv.setText("");
                searchDataAdapter=new SearchDataRVAdapter(SearchDataActivity.this,selectedItemList);
                selectedDataRv.setLayoutManager(new LinearLayoutManager(SearchDataActivity.this,LinearLayoutManager.VERTICAL,false));
                selectedDataRv.setHasFixedSize(true);
                selectedDataRv.setAdapter(searchDataAdapter);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        searchDataAdapter.notifyDataSetChanged();
                    }
                });
            }
        });

    }
}
