package com.search.nav.app.searchdialogbox.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.search.nav.app.searchdialogbox.model.UserDataModel;

import java.util.ArrayList;


public class DatabaseBackend extends SQLiteOpenHelper {
    private static DatabaseBackend instance = null;

    private static final String DATABASE_NAME = "TakeAPledge";
    private static final int DATABASE_VERSION = 3;

    public static String CREATE_PRODUCT_STATEMENT = "create table "
            + UserDataModel.TABLE_NAME + "("
            + UserDataModel.NAME + " TEXT " +
            ");";

    public DatabaseBackend(Context context) {
        super(context,  DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_PRODUCT_STATEMENT);
    }

    public static synchronized DatabaseBackend getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseBackend(context);
        }
        return instance;
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("version",""+oldVersion+"--------"+newVersion);
        db.execSQL("DROP TABLE IF EXISTS " + UserDataModel.TABLE_NAME);
        db.execSQL(CREATE_PRODUCT_STATEMENT);
    }

    public void CreateUserForm(UserDataModel userDataModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(userDataModel.TABLE_NAME, null, userDataModel.getContentValues());
    }
    public ArrayList<UserDataModel> getUserData(String username) {
        ArrayList<UserDataModel> list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor;
        cursor=db.rawQuery( "SELECT  * FROM " + UserDataModel.TABLE_NAME + " where name  LIKE '%"+username+"%'",null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                UserDataModel message = UserDataModel.fromCursor(cursor);
                list.add(message);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return list;
    }

}
