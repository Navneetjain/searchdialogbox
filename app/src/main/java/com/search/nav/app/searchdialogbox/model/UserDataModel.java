package com.search.nav.app.searchdialogbox.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.search.nav.app.searchdialogbox.helper.AbstractEntry;

/**
 * Created by i5tagbin2 on 12/9/17.
 */

public class UserDataModel extends AbstractEntry {
    public static final String TABLE_NAME="userData";
    public static final String NAME="name";

    String name;

    public UserDataModel(String name){
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues contentValues=new ContentValues();
        contentValues.put(NAME,getName());
        return contentValues;
    }
    public static UserDataModel fromCursor(Cursor cursor) {
        return new UserDataModel(cursor.getString(cursor.getColumnIndex(NAME)));
    }
}
