package com.search.nav.app.searchdialogbox.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.search.nav.app.searchdialogbox.R;
import com.search.nav.app.searchdialogbox.database.DatabaseBackend;
import com.search.nav.app.searchdialogbox.model.UserDataModel;

public class MainActivity extends AppCompatActivity {
    EditText editName;
    Button saveButton;
    DatabaseBackend databaseBackend;
    TextView nextActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editName= (EditText) findViewById(R.id.name);
        saveButton= (Button) findViewById(R.id.saveButton);
        nextActivity= (TextView) findViewById(R.id.nextActivity);
        SpannableString content = new SpannableString("next >");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        nextActivity.setText(content);
        nextActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,SearchDataActivity.class);
                startActivity(intent);
            }
        });
        databaseBackend=DatabaseBackend.getInstance(this);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editName.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this,"Please enter any Name",Toast.LENGTH_SHORT).show();
                }else {
                    UserDataModel userDataModel=new UserDataModel(editName.getText().toString());
                    databaseBackend.CreateUserForm(userDataModel);
                    Toast.makeText(MainActivity.this,"Data saved Successfully",Toast.LENGTH_SHORT).show();
                    editName.setText("");
                }
            }
        });
    }
}
